var builder = WebApplication.CreateBuilder(args);
var serverName = "broker";
var coordinateAdress = "http://register-container:7085";

// Add services to the container.
builder.Services.AddRazorPages();
var app = builder.Build();

try
{
    Console.WriteLine("Lancement du serveur Broker ...");
    await RegisterServer(coordinateAdress, serverName);



    app.MapGet("/broker/{adressToCall}/{message}", async context =>
    {    
        var adresse = context.Request.RouteValues["adressToCall"]?.ToString();
        var message = context.Request.RouteValues["message"]?.ToString();
        var completeAdress = "http://" + adresse;


        Console.WriteLine($"Broker got request from {completeAdress} with message : {message}");

        using (var client = new HttpClient())
        {
            await client.GetStringAsync($"{completeAdress}");
        }

        await context.Response.WriteAsync("Pong");
    });
}
catch (HttpRequestException ex)
{

    Console.WriteLine($"Erreur lors de la requête HTTP : {ex.Message}");

}
catch (Exception ex)
{
    Console.WriteLine($"Erreur inattendue : {ex.Message}");

}

async Task RegisterServer(object coordinateAdress, string serverName)
{
    using (var client = new HttpClient())
    {
        var response = await client.GetAsync($"{coordinateAdress}/register/{serverName}/broker-container:5030");
        response.EnsureSuccessStatusCode();
    }

    Console.WriteLine("Serveur Broker est enregistre avec succès ...");
}

app.UseHttpsRedirection();
app.UseStaticFiles();

app.UseRouting();

app.UseAuthorization();

app.MapRazorPages();

app.Run();
