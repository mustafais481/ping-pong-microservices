using Newtonsoft.Json;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddRazorPages();

var app = builder.Build();
var registerService = new RegisterService();

app.MapGet("/register/{serverName}/{adress}", context =>
{
    var serverName = context.Request.RouteValues["serverName"]?.ToString();
    var adresse = context.Request.RouteValues["adress"]?.ToString();

    var completedAdress = "http://" + adresse;

    Console.WriteLine($"Register got a request from {serverName} with the adresse : {adresse}");

    if (!string.IsNullOrEmpty(serverName) && !string.IsNullOrEmpty(adresse))
    {
        registerService.EnregistrerAdresse(serverName, completedAdress);
        Console.WriteLine($"Adresse enregistree pour {serverName} avec l'adress {completedAdress}");
        return context.Response.WriteAsync($"Adresse enregistree pour {serverName}");
    }
    else
    {
        return context.Response.WriteAsync("Paramètres invalides");
    }
});

app.MapGet("/get/{serverName}", context =>
{
    var nomServeur = context.Request.RouteValues["serverName"]?.ToString();
    var adresses = registerService.ObtenirAdressesParServeurName(nomServeur);


    if (adresses.Any())
    {

        var len = adresses.Count();
        Console.WriteLine($"Il y a {len} elements");

        var response = new { Adresse = adresses };
        var jsonResponse = JsonConvert.SerializeObject(response);
        context.Response.ContentType = "application/json";
        return context.Response.WriteAsync(jsonResponse);
    }
    else
    {
        return context.Response.WriteAsync("Serveur non enregistré");
    }
});
     

app.UseHttpsRedirection();
app.UseStaticFiles();

app.UseRouting();

app.UseAuthorization();

app.MapRazorPages();

app.Run();
