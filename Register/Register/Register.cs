﻿public class RegisterService
{
    private Dictionary<string, List<string>> adresses = new Dictionary<string, List<string>>();

    public void EnregistrerAdresse(string nomServeur, string adresse)
    {
        if (adresses.ContainsKey(nomServeur))
        {
            adresses[nomServeur].Add(adresse);
        }
        else
        {
            adresses[nomServeur] = new List<string> { adresse };
        }
    }

    public List<string> ObtenirAdressesParServeurName(string serveurName)
    {
        if (adresses.TryGetValue(serveurName, out var adressesList))
        {
            return adressesList;
        }
        else
        {
            return new List<string>();
        }
    }

}

