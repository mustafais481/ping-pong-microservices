using Docker.DotNet;
using Docker.DotNet.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

var builder = WebApplication.CreateBuilder();
var app = builder.Build();

var coordinateAdress = "http://register-container:7085";
var ping = "ping";
var broker = "broker";

string containerId = Environment.GetEnvironmentVariable("HOSTNAME");

Console.WriteLine("Id du conteneur : " + containerId);

try
{
    var currentContainerName = await GetContainerNameById(containerId!);

    Console.WriteLine("Nom du conteneur actuel : " + currentContainerName);

    await RegisterServer(coordinateAdress, ping, currentContainerName);

    app.MapGet("/", async context =>
    {
        Console.WriteLine("Ping server want to send ping");
        var adressBroker = await GetBrokerServerAdresseFromRegisterServer(coordinateAdress, broker);
        Console.WriteLine("adress broker : " + adressBroker);

        var adressToCall = await GetAdressToCall(coordinateAdress, currentContainerName);
        Console.WriteLine("adressToCall : " + adressToCall);

        using (var client = new HttpClient())
        {
            Console.WriteLine("Lancement du serveur Broker pour envoyer le message pong au serveur 2");

            await client.GetStringAsync($"{adressBroker}/broker/{adressToCall}/ping");
        }

        Console.WriteLine("Ping server sent Ping");

        await context.Response.WriteAsync("Pong");
    });
}
catch (HttpRequestException ex)
{

    Console.WriteLine($"Erreur lors de la requête HTTP : {ex.Message}");

}
catch (Exception ex)
{
    Console.WriteLine($"Erreur inattendue : {ex.Message}");

}

async Task<string> GetContainerNameById(string targetContainerId)
{
    using (var client = new DockerClientConfiguration(new Uri("unix:///var/run/docker.sock")).CreateClient())
    {
        var containers = await client.Containers.ListContainersAsync(new ContainersListParameters { All = true });

        var container = containers.FirstOrDefault(x => x.ID.Length >= targetContainerId.Length && x.ID.Substring(0, targetContainerId.Length) == targetContainerId);
        if (container != null)
        {
            return container.Names.FirstOrDefault()?.TrimStart('/');
        }
        else
        {
            Console.WriteLine($"Aucun conteneur trouvé avec l'ID {targetContainerId}");

            foreach(var c in containers)
            {
                Console.WriteLine($"Container ID: {c.ID}");
                Console.WriteLine($"Container Name: {c.Names.FirstOrDefault()}");
                Console.WriteLine($"Container Image: {c.Image}");
                Console.WriteLine();
            }

            return null;
        }
    }
}

async Task<string> GetBrokerServerAdresseFromRegisterServer(string coordinateurAdresse, string serverName)
{
    using (var client = new HttpClient())
    {
        var getResponse = await client.GetAsync($"{coordinateurAdresse}/get/broker");
        getResponse.EnsureSuccessStatusCode();

        var content = await getResponse.Content.ReadAsStringAsync();
        var jsonResponse = JsonConvert.DeserializeObject<dynamic>(content);
        Console.WriteLine("JSON Response: " + jsonResponse);
        string adresse = jsonResponse["Adresse"][0].ToString();

        return adresse;
    }
}

async Task RegisterServer(string coordinateurAdresse, string serverName, string currentContainerName)
{
    using (var client = new HttpClient())
    {
        var response = await client.GetAsync($"{coordinateurAdresse}/register/{serverName}/{currentContainerName}:7100");
        response.EnsureSuccessStatusCode();
        Console.WriteLine("Service Ping enregistre avec succes : " + response.StatusCode );
    }
}

async Task<string> GetAdressToCall(string coordinateurAdresse, string currentContainerName)
{
    using (var client = new HttpClient())
    {
        var getResponse = await client.GetAsync($"{coordinateurAdresse}/get/pong");
        getResponse.EnsureSuccessStatusCode();
        Console.WriteLine("Service Recupere enregistre avec succes : " + getResponse.StatusCode);

        var content = await getResponse.Content.ReadAsStringAsync();
        JObject jsonResponse = JsonConvert.DeserializeObject<dynamic>(content);
        List<string> adresses = jsonResponse["Adresse"].Select(a => a.ToString()).ToList();

        var address = GetAdressFromList(adresses, currentContainerName);

        Console.WriteLine("adress : " + address.Result);
        Uri uri = new Uri(address.Result);
        string hostAndPort = uri.Host + ":" + uri.Port;

        return hostAndPort;
    }
}

static Task<string> GetAdressFromList(List<string> adresses, string currentContainerName)
{
    int lastDashIndex = currentContainerName.LastIndexOf('-');

    if (lastDashIndex != -1 && lastDashIndex < currentContainerName.Length - 1)
    {
        string containerNumber = currentContainerName.Substring(lastDashIndex + 1);

        string result = adresses.FirstOrDefault(adresse => adresse.Contains(containerNumber));

        if (result != null)
        {
            return Task.FromResult(result);
        }
        else
        {
            Console.WriteLine("Aucune correspondance trouvée dans la liste d'adresses.");
        }
    }
    else
    {
        Console.WriteLine("Format non valide pour le nom du conteneur.");
    }

    return Task.FromResult<string>(null); // Ou retournez une chaîne vide, selon votre logique
}


await app.RunAsync();

